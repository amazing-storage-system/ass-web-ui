import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import snack from './modules/snack';
import containers from './modules/containers';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    containers,
    snack,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
