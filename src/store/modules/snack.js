/* eslint-disable no-param-reassign */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-shadow */

const state = {
  snacks: [],
  snackbar: false,
};

const getters = {

};

const actions = {

};

const mutations = {
  showSnackbar(state, payload) {

    var snackbar = {};
    snackbar.text = payload.text;
    snackbar.multiline = (payload.text.length > 50);

    if (payload.multiline) {
      snackbar.multiline = payload.multiline;
    }

    if (payload.timeout) {
      snackbar.timeout = payload.timeout;
    }

    snackbar.visible = true;

    if(!state.snackbar && !state.snacks) {
      snackbar = this.snacks.shift()
      this.$nextTick(() => this.snackbar=snackbar)
    }

    state.snacks.push(snackbar);
  },
  closeSnackbar(state) {
    //state.snackbar.visible = false;
    //state.snackbar.multiline = false;
    //state.snackbar.timeout = 6000;
    //state.snackbar.text = null;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

