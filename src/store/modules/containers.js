/* eslint-disable no-param-reassign */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-shadow */

import axios from 'axios';
import i18n from '../../i18n';

const state = {
  loadingContainers: false,
  loadingContainer: false,
  container: {},
  containers: [],
  editdialog: false,
  createdialog: false,
  savingContainer: false,
  savingError: false,
};

const getters = {

};

const actions = {
  fetchContainer(store) {

    store.commit('GET_CONTAINER_PENDING');
    return axios.get('api/container/')
      .then((response) => {
        store.commit('GET_CONTAINER_SUCCESS', response.data);
      })
      .catch((error) => {
        store.commit('GET_CONTAINER_FAILURE', error);
      });
  },
  fetchContainers(store) {

    store.commit('snack/showSnackbar', { text: i18n.t('container-added-success') }, { root: true });
    store.commit('snack/showSnackbar', { text: 'pwet' }, { root: true });

    store.commit('GET_CONTAINERS_PENDING');
    return axios.get('api/containers')
      .then((response) => {
        store.commit('GET_CONTAINERS_SUCCESS', response.data);
      })
      .catch((error) => {
        store.commit('GET_CONTAINERS_FAILURE', error);
      });
  },
  addContainer(store, item) {
    store.commit('ADD_CONTAINER_PENDING');
    return axios.post('api/containers/create', item)
      .then((response) => {
        store.commit('ADD_CONTAINER_SUCCESS', response.data);

      })
      .catch((error) => {
        store.commit('ADD_CONTAINER_FAILURE', error);
      });
  },
  openCreateDialog(store) {
    store.commit('OPEN_CREATE_DIALOG');
  },
  closeCreateDialog(store) {
    store.commit('CLOSE_CREATE_DIALOG');
  },
};

const mutations = {

  OPEN_CREATE_DIALOG(state) {
    state.createdialog = true;
  },
  CLOSE_CREATE_DIALOG(state) {
    state.createdialog = false;
  },
  OPEN_EDIT_DIALOG(state) {
    state.editdialog = true;
  },
  CLOSE_EDIT_DIALOG(state) {
    state.editdialog = false;
  },
  ADD_CONTAINER_PENDING(state) {
    state.savingError = false;
    state.savingContainer = true;
  },
  ADD_CONTAINER_SUCCESS(state, container) {
    state.savingContainer = false;
    state.savingError = false;
    state.dialog = false;
    state.containers.push(container);
  },
  ADD_CONTAINER_FAILURE(state, err) {
    state.savingContainer = false;
    state.savingError = true;
    //console.error(err);
  },
  GET_CONTAINER_PENDING(state) {
    state.loadingContainer = true;
  },
  GET_CONTAINERS_PENDING(state) {
    state.loadingContainers = true;
  },
  GET_CONTAINER_SUCCESS(state, container) {
    state.loadingContainer = false;
    state.container = container;
  },
  GET_CONTAINERS_SUCCESS(state, containers) {
    state.loadingContainers = false;
    state.containers = containers;
  },
  GET_CONTAINER_FAILURE(state, err) {
    state.loadingContainer = false;
    console.error(err);
  },
  GET_CONTAINERS_FAILURE(state, err) {
    state.loadingContainers = false;
    console.error(err);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

