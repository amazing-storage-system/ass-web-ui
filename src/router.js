import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/testpictureattachments',
      name: 'testpictureattachments',
      component: () => import(/* webpackChunkName: "testpictureattachments" */ './views/TestPictureAttachments.vue'),
    },
    {
      path: '/testfileattachments',
      name: 'testfileattachments',
      component: () => import(/* webpackChunkName: "testfileattachments" */ './views/TestFileAttachments.vue'),
    },
    {
      path: '/search',
      name: 'search',
      component: () => import(/* webpackChunkName: "search" */ './views/Search.vue'),
    },
    {
      path: '/perishables',
      name: 'perishables',
      component: () => import(/* webpackChunkName: "perishables" */ './views/Perishable.vue'),
    },
    {
      path: '/stocks',
      name: 'stocks',
      component: () => import(/* webpackChunkName: "stocks" */ './views/Stocks.vue'),
    },
    {
      path: '/orders',
      name: 'orders',
      component: () => import(/* webpackChunkName: "orders" */ './views/Orders.vue'),
    },
    {
      path: '/containers/view',
      name: 'containers',
      component: () => import(/* webpackChunkName: "containers" */ './views/Containers.vue'),
    },
    {
      path: '/items/view',
      name: 'items',
      component: () => import(/* webpackChunkName: "items" */ './views/Items.vue'),
    },
    {
      path: '/events/view',
      name: 'events',
      component: () => import(/* webpackChunkName: "events" */ './views/Events.vue'),
    },
  ],
});
