module.exports = {
  containers: [
    {
      type: 'lieu',
      id: 'REMISEATELIER#01',
      description: "remise principale de l'atelier",
      tags: [
        'atelier',
        'lieu',
        'remise',
      ],
      parent: null,
      movable: false,
    },
    {
      type: 'caisse',
      id: 'BOITE#DLS',
      idmeta: {
        iata: 'DLS',
        country: 'united states',
        city: 'dallas',
      },
      label: 'caisse solex',
      description: 'boite contenant des pieces en rapport avec le solex',
      tags: [
        'pieces',
        'solex',
      ],
      parent: 'REMISEATELIER#01',
    },
    {
      type: 'sachet',
      id: 'SACHET#SOLEX#001',
      label: 'joints de culasse solex',
      description: 'sachet contenant des joints de culasse de solex',
      tags: [
        'joints',
        'culasse',
        'solex',
      ],
      parent: 'BOITE#DLS',
    },
  ],
  items: [
    {
      id: 'RANDOMID12178234098',
      label: 'joint culasse',
      description: 'joint de culasse solex 3800',
      qty: 10,
      container: 'SACHET#SOLEX#001',
      tags: [
        'joint',
        'culasse',
      ],
      picture: 'https://i.imgur.com/olXphSg.png',
      meta: {

      },
      locations: [
        'REMISEATELIER#01',
        'BOITE#DLS',
        'SACHET#SOLEX#001',
      ],
    },
  ],
};
