require('dotenv').config();

module.exports = {

  // api proxy
  devServer: {
    proxy: {
      '/api': {
        target: process.env.API_SERVER_URL || 'http://localhost:3000/',
        ws: true,
        changeOrigin: true,
      },
    },
  },

  // plugins
  pluginOptions: {
    i18n: {
      locale: 'fr',
      fallbackLocale: 'fr',
      localeDir: 'locales',
      enableInSFC: true,
    },
  },
};

